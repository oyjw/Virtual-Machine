#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_
#include <exception>
#include <string>

class Error :public std::exception
{
    std::string str;
public:
    Error(const std::string& msg):str(msg) {}
    virtual const char *what() const
#ifndef WIN32
noexcept (true)
#endif
    {
        return str.c_str();
    }
};

class TypeError :public Error
{
public:
	TypeError(const std::string& msg) :Error(msg) {}
};

class AttrError :public Error
{
public:
	AttrError(const std::string& msg) :Error(msg) {}
};

class TokenError :public Error
{
public:
	TokenError(const std::string& msg) :Error(msg) {}
};

class SyntaxError :public Error
{
public:
	SyntaxError(const std::string& msg) :Error(msg) {}
};

class SymbolError :public Error
{
public:
	SymbolError(const std::string& msg) :Error(msg) {}
};

class ArgumentError :public Error
{
public:
	ArgumentError(const std::string& msg) :Error(msg) {}
};

class MemoryError :public Error
{
public:
	MemoryError(const std::string& msg) :Error(msg) {}
};
#endif
